---
title: "Ohjelmistoympäristön asennus ja määrittely - oma kone"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---



![](http://courses.markuskainu.fi/utur2016/kuviot/ymparisto_2.png)

**Kuvio:** *Kurssin ohjelmistoympäristö*

Ylläolevassa kuvassa on kuvattu ohjelmistoympäristön kaksi eri vaihtoehtoa: 1) ohjelmat CSC:n serverillä, 2) ohjelmat omalla koneella.


<div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h1>Kurssimateriaalien "forkkaaminen" Gitlabissa</h1>
  
  <div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Tehdään vain ensimmäisellä kerralla!</strong>
  </div>
  
  <ul>
  <li>1 Siirry osoitteesee: <https://gitlab.com/utur2016/content> ja klikkaa ruudun keskeltä **Fork**</li>
  <li>2 Valitse oma profiilisi, klikkaa **Fork** ja odota hetki</li>
  <li>3 Valitse oikealta hammasratas-ikoni ja sen alta **Members**</li>
  <li>4 kirjoita **People**-kenttään `muuankarski` ja valitse *Markus Kainu* ja anna hänelle *developer*-oikeudet!</li>
  </ul>
  </br>
  <a href="./videot/utur2016_ohjelmistoymparisto1_2.mp4" class="alert-link">Katso video</a>

</div>


<!-- <!--html_preserve--> -->
<!-- <video id="sampleMovie" width="640" height="360" preload controls> -->
<!-- 	<source src="./videot/utur2016_ohjelmistoymparisto1_2.mp4" /> -->
<!-- 	<source src="./videot/utur2016_ohjelmistoymparisto1_2.webm" /> -->
<!-- </video> -->
<!-- <!--/html_preserve--> -->


# Uuden luennon tai kotitehtäväsession alussa

Jos olet [asentanut ohjelmat](), niin käynnistä Rstudio ja avaa projekti `content`

<!-- <div class="alert alert-dismissible alert-danger"> -->
<!--   <button type="button" class="close" data-dismiss="alert">&times;</button> -->
<!--   <strong>Varapalvelin!</strong> -->

<!--   - Palvelimen osoite <http://xx.181.168.173/> -->
<!--   - tunnus: `utu_tunnus` -->
<!--   - salasana: rstudio -->

<!-- </div> -->


# Rstudion & gitlab:n määritykset {.tabset}

## Oma kone ensimmäisellä kerralla

### gitin konffaaminen Rstudiossa

1. Avaa pääte (**Tools** -> **Shell**) ja kopioimaan käskyt ilman `system()` funktioita päätteeseen. 

```{bash, eval=FALSE}
# Vaihda skriptiin omat tietosi!
git config --global user.name "Etunimi Sukunimi"
git config --global user.email "utu_tunnus@utu.fi"
```

2. Luo [rsa-avain](https://fi.wikipedia.org/wiki/RSA) valitsemalla **Tools** -> **Global Options** -> **Git/SVN** -> *Create RSA Key* (Ei tarvita salasanaa tällä kurssilla!) 
3. Klikkaa **View public key** ja kopioi se leikepöydälle (Ctrl+c)

- [Katso video](./videot/utur2016_ohjelmistoymparisto1_1.mp4)

### Gitlabin konffaaminen

Siirry selaimella [gitlabiin](https://gitlab.com/users/sign_in) ja 

**session ssh-avaimen lisääminen**

1. klikkaa oikealta **profile settings** -> **SSH keys**.
2. Kopioi isompaan ruutuun julkinen ssh-avain (`Ctrl+v`), hyväksy oletusotsikko ja klikkaa **Add key**


### Rstudion konffaaminen

1. Siirry omaan projektiisi <https://gitlab.com/utu_tunnus/content>
2. Kopioi uuden projektin ssh-osoite ruudun keskeltä joka muotoa: `git@gitlab.com:utu_tunnus/content.git` ja siirry takaisin Rstudioon
3. Klikkaa oikealta **Project: (None)** ja valitse **New Project** -> **Version Control** -> **Git** ja annan kenttään **repository URL** ssh-osoite ja klikkaa **Create Project**
4. Rstudio lataa gitlab-projektista tiedostot Rstudio-projektiin ja voit aloittaa R:n käytön! 
5. Asenna istunnon aluksi aina uusin versio **tidyverse**-paketeista komennolla `install.packages("tidyverse")`

- [Katso video](./videot/utur2016_ohjelmistoymparisto1_3.mp4)


## Oma kone jatkossa

Jos olet [asentanut ohjelmat](), niin käynnistä Rstudio ja avaa projekti `content`

# Kurssimateriaaliin tulleiden muutosten päivittäminen

Kurssin materiaalien sisältö muuttuu koko ajan. Kotitehtävien sisältö EI muutu enää kun kotitehtävä on julkaistu. Saadaksesi uusimmat päivitykset ennen kun aloitat kotitehtävän tekemisen tai luennon alussa niin , käynnistä ensin pääte (**Tools** -> **Shell**) ja kopioi alla olevat `git`-komennot siihen:


```{bash, eval=FALSE}
git remote add upstream git@gitlab.com:utur2016/content.git # lisää alkuperäinen upstreamiksi
git fetch upstream # vedä muutokset alkuperäisestä koneellesi upstream/master
# JOS et ole tehnyt TULEVIIN luentoihin/kotitehtäviin muutoksia niin aja
git reset --hard # poista mahdolliset ei-committatut muutokset
git merge upstream/master # yhdistä upstreamista vedetyt muutokset nykyiseen master-branchiin
# JOS taas olet tehnyt muutoksia, niin aja
git rebase upstream/master
```


<div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Virhe: running command 'git fetch upstream' had status 128</strong>
  
  Monilla oman koneen käyttäjillä on ollut ongelmia materiaalin päivittämisessä ja komentojen kuten `system("git fetch upstream")` on antanut virheilmoituksia tyyliin `running command 'git fetch upstream' had status 128`. 
  
  Helpoin tapa ohittaa nämä virheet on välttää `system("")`-käskyjen antamista ja käynnistää ensin pääte (**Tools** -> **Shell**) ja kopioida `git`-komennot siihen **ilman** `system("")`-funktiota, eli kuten tämän sivun ohjeissa neuvotaan!

</div>





Mikäli tuntuu siltä että olet sotkenut alkuperäisen materiaalin kokonaan ja haluaisit aloittaa ns. puhtaalta pöydältä voi menetellä seuraavien ohjeiden mukaan, jossa siis 

1. varmuuskopioidaan projektin tämänhetkinen tila uuteen haaraan (branch) ja 
2. resetoidaan projektin master-haara identtiseksi upstreamin eli `utur2016/content`-projektin kanssa. 

Avaa pääte ja kopioi seuraavat käskyt:

```{bash, eval=FALSE}
git commit -am "committaan vanhat sotkut varmuuden vuoksi" # nykyisten muutosten committaus
git branch vanha_master # nykyinen projektin haaraan vanha_master
git checkout vanha_master # Vaihdetaan tähän uuteen haaraan
git push origin vanha_master # siirretään se gitlabiin turvaan varmuuden vuoksi
git remote add upstream git@gitlab.com:utur2016/content.git # lisää alkuperäinen upstreamiksi varmuuden vuoksi
git fetch upstream # vedä muutokset alkuperäisestä koneellesi upstream/master
git checkout master # varmistetaan että ollaan masterissa
git reset --hard upstream/master # resetoidaan upstreamin kanssa
git push origin master --force # korvataan gitlab:ssa entinen master tällä uudella 
```

# Kotitehtävien tekeminen ja muutosten tallentaminen Gitlabiin session päätteksi

**Metodi 1 - IDE:n avulla**

1. mene Rstudiossa **git** välilehdelle ja 
    1. klikkaa tiedostot joiden muutokset haluat saada talteen ja klikkaa **Commit**
    2. kirjoita ruutuun **muutoksia kuvaava viesti sekä syy muutoksille** ja klikkaa **Commit*
    3. Sitten klikkaa ensin **Pull** ja anna ssh-salasanasi 
    4. Sitten klikkaa **Push**
4. Sulje Rstudio tai aloita jo kotitehtävät!

**Metodi 2 - koodilla**

Avaa pääte ja kopioi seuraavat komennot:

```{bash eval=FALSE}
git commit -am "tein kotitehtäviä ja annan tähän runsaan kuvauksen siitä mitä tein, mikä onnistui ja mikä oli vaikeaa"
git pull origin master
git push origin master
```





- [Katso video](./videot/utur2016_ohjelmistoymparisto1_4.mp4)


</br>

***

</br>


# Ohjelmistoympäristön asentaminen omalle koneelle

Asennettuna pitää olla R, RStudio sekä git. Seuraavassa ohjeet miten se tapahtuu eri käyttöjärjestelmiin:

## R:n asentaminen

Noudata asennusohjeita omalle käyttöjärjestelmälle:

- Download R for Linux <http://ftp.acc.umu.se/mirror/CRAN/bin/linux/>
    - Tarkemmat ohjeet Ubuntuun (Mintiin tms.): <https://cran.rstudio.com/bin/linux/ubuntu/>
- Download R for (Mac) OS X <http://ftp.acc.umu.se/mirror/CRAN/bin/macosx/>
    - FAQ <https://cran.r-project.org/bin/macosx/RMacOSX-FAQ.html>
- Download R for Windows <https://cran.r-project.org/bin/windows/base/>
    - R for Windows FAQ <https://cran.r-project.org/bin/windows/base/rw-FAQ.html>
	 
Mikäli olet kiinnostunut pakettien kirjoittamisesta tai yleisemmin ohjelmoinnista R:llä sinun tulee asentaa myös Macille/Windowsiin vaadittavat kehitystyökalut

- Windows: Rtools <http://ftp.acc.umu.se/mirror/CRAN/bin/windows/Rtools/>
- R for Mac OS X: Development Tools and Libraries   <http://ftp.acc.umu.se/mirror/CRAN/bin/macosx/tools/>
	 
## Rstudion asentaminen

Lataa omalle käyttöjärjestelmälle sopiva versio [Rstudion sivuilta](https://www.rstudio.com/products/rstudio/download/)

Mikäli haluat käyttää kaikkia blogosfäärissä hehkutettuja uusia ominaisuuksia, niin asenna silloin *preview*-versio osoitteesta <https://www.rstudio.com/products/rstudio/download/preview/>. Myös preview-versiot ovat olleet hyvin vakaita.

## Gitin asentaminen

Lataa omalle käyttöjärjestelmälle sopiva versio Gitin sivuilta <https://git-scm.com/>

Konfiguroi git heti asentamisen jälkeen

1. etsi ohjelma **Git Bash** Windowsissa ja ihan tavallinen pääte (**Shell**) Macillä tai linuxissa
2. määritä käyttäjänimesi komennolla `git config --global user.name "Minun Nimeni"`
3. määritä sähköpostiosoitteese komennolla `git config --global user.email "minun.sahkopostini@mail.com"`


## Asennuksen testaus

1. Avaa Rstudio ja oikealta ylhäältä
2. Klikkaa oikealta **Project: (None)** ja valitse **New Project** -> **Version Control** -> **Git** ja annan kenttään `git@gitlab.com:utur2016/blank.git` ssh-osoite ja klikkaa **Create Project**
3. Aja R:ssä komento `source("setup.R")`
4. Virheilmoituksia ei pitäisi tulla, vaan pitäisi rakentua uuden projektin hakemistorakenne ja kaikenlaisia pieniä tiedostoja!

**Kysymykset asennukseen liittyen tänne: <https://utur2016.slack.com/messages/softat_omalle_koneell/>**

