#' ---
#' title: "Kotitehtävä 3"
#' author: utu_tunnus
#' output:
#'   html_document:
#'     toc: true
#'     toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session3_transform_kotitehtava.R)

#+ setup
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=FALSE,message=FALSE))

#' # Kansiorakenteen luominen
#' 
#' R:ssä on kommennot käyttöjärjestelmän tiedostojärjestelmän käyttöön, kuten tiedostojen luomiseen (`file.create()`) 
#' kansioiden luomiseen (`dir.create()`).
#' 
#' **Millä komennolla luot nykyisen työhakemistoon kansion `kotitehtava3`?**
#+ vastaus1
plot(cars)

#' 
#' # HArjoitusdatojen luominen koneella ja tallentaminen uuteen kansioon
#' 
#' Alla oleva skripti luo viisi uutta `tibble`objektia `cases`,`pollution`,`storms`,`songs` ja `artists`. Luo ensin objektit
#' ajamalla koodi.  

library(tidyverse)
# cases-data
cases <- dplyr::data_frame(country = c("FR", "DE", "US", "FR", "DE", "US", "FR", "DE", "US"),
                           year = c(2011,2011,2011,2012,2012,2012,2013,2013,2013),
                           n = c(7000,5800,15000,6900,6000,14000,7000,6200,13000)) %>%  
  spread(., year, n)
cases
# pollution-data
pollution <- dplyr::data_frame(city = c("New York", "New York", "London", "London", "Beijing", "Beijing"),
                               size = c("large", "small", "large", "small", "large", "small"),
                               amount = c(23,14,22,16,121,56))
pollution
# storms
storms <- dplyr::data_frame(storm = c("Alberto", "Alex", "Allison", "Ana", "Arlene", "Arthur"),
                            wind = c(110,45,65,40,50,45),
                            pressure = c(1007,1009,1005,1013,1010,1010),
                            date = as.Date(c("2000-08-03", "1998-07-27", "1995-06-03", "1997-06-30", "1999-06-11", "1996-06-17")))
storms
# songs-data
songs <- dplyr::data_frame(song = c("Across the Universe", "Come Together", "Hello, Goodbye", "Peggy Sue"),
                           name = c("John", "John", "Paul", "Buddy"))
songs
# artists
artists <- dplyr::data_frame(name = c("George", "John", "Paul", "Ringo"),
                             plays = c("sitar", "guitar", "bass", "drums"))
artists


#' # Datojen tallentaminen R-muotoon (RDS)
#'  
#' Datojen luomisen jälkeen **tallenna kukin data erikseen R:n formaatissa (RDS) edellisessä kohdassa luomaasi kansioon** funktiolla `saveRDS()` tyyliin
#' `saveRDS(object = data, file = "./kotitehtava3/data.RDS")`. (Myöhemmin voit ladata datat käyttöön tyyliin `data <- readRDS(file = "./kotitehtava3/data.RDS")`
#' #' 
#+ vastaus2
plot(cars)

#' # Datojen lukeminen R-muodosta (RDS)
#' 
#' Vaikka datat ovatkin jo nyt sinun R-sessiossa objekteina. Kirjoita vielä ylläolevin ohjeiden pohjalta **miten kunkin 
#'  datan ladattua kansiosta `kotitehtava3`**. 
#' 
#+ vastaus3
plot(cars)

#' # Datojen levittäminen ja kokoaminen tidyr-paketilla
#' 
#' Otetaan data `pollution` - **miten teet siitä leveän niin että se näyttää tältä**
#' 
##       city large small
## 1  Beijing   121    56
## 2   London    22    16
## 3 New York    23    14
#+ vastaus311
plot(cars)


#' Otetaan data `cases` - **miten teet siitä pitkän niin että se näyttää tältä**
#' 
##   country year     n
## 1      DE 2011  5800
## 2      FR 2011  7000
## 3      US 2011 15000
## 4      DE 2012  6000
## 5      FR 2012  6900
## 6      US 2012 14000
## 7      DE 2013  6200
## 8      FR 2013  7000
## 9      US 2013 13000
#+ vastaus312
plot(cars)


#' # Datan filtteroiminen 
#' 
#' Käytetään dataa `storms`. **Valitse datasta kaikki muuttujat paitsi `storm`**
#' 
#+ vastaus4
plot(cars)



#' Käytetään dataa `storms`. **Valitse datasta rivit, joilla tuulennopeus on ollut yhtäsuuri tai yli 50**
#' 
#+ vastaus5
plot(cars)


#' Käytetään dataa `storms`. **Valitse datasta rivit, joilla joko tuulennopeus on ollut yhtäsuuri tai yli 40
#' TAI joiden nimi on joko `Alberto` tai  `Alex`. R:ssä `OR` operaattori on |.**
#' 
#+ vastaus6
plot(cars)


#' ## Muutama hyödyllinen **lisäverbi** dplyr-paketista ja niiden käyttö
#' 
#' Funktio `contains()` valitsee muuttujat, joiden nimissä on tietty merkkijono. Muuttujia valitessa ko. 
#' funktiota käytetään siis `select()`-funktion sisässä.
#' 
#' **Valitse storms-datasta muuttujat joiden muuttujanimessä on kirjain `t`**
#+ vastaus21
plot(cars)


#' Funktiot `starts_with()` ja `ends_with()` puolestaa valitsevat muuttujat, joiden nimi joko alkaa tai päättyy
#' tiettyyn merkkijonoon. Kuten edellä, funktiota käytetään `select()`-funktion sisässä.
#' 
#' **Valitse storms-datasta muuttujat jotka päättyvät kirjaimeen `e`.**
#+ vastaus22_1
plot(cars)

#' Selecting kanssa voit käyttää myös mm. seuraavia "verbejä"
#' `everything()` # Select every column
#' `matches()` # Select columns whose name matches a regular expression
#' `num_range()` # Select columns named x1, x2, x3, x4, x5
#' `one_of()` # Select columns whose names are in a group of names
#' 
#' **Käytä jotain em. verbeistä muuttujien valitsemiseen `pollution`-datasta**
#+ vastaus22
plot(cars)


#' # mutate () - uusien muuttujien luominen
#' 
#' Käytetään vielä `storms`-dataa. **Luo uusi muuttuja `ratio` joka on ilmanpaineen ja tuulennopeuden suhde**
#+ vastaus23
plot(cars)


#'  Alla on lisää näppäriä mutate()-funktion kanssa käytettäviä lisäfunktioita
#'  
#'  pmin(), pmax() # Element-wise min and max
#'  cummin(), cummax() # Cumulative min and max
#'  cumsum(), cumprod() # Cumulative sum and product
#'  between() # Are values between a and b?
#'  cume_dist() # Cumulative distribution of values
#'  cumall(), cumany() # Cumulative all and any
#'  cummean() # Cumulative mean
#'  lead(), lag() # Copy with values one position
#'  ntile() #Bin vector into n buckets
#'  dense_rank(), min_rank(), percent_rank(), row_number() # Various ranking methods
#'  
#'  **Käyttäen `storms`-dataa laske uusi muuttuja `kumutuuli` jossa on kumulatiivinen tuulennopeus**
#+ vastaus24
plot(cars)


#' # Yhteenveto summarise()-funktiolla
#' 
#' Käytetään `pollution` dataa. **Summaa data kahdeksi muuttujaksi `mediaani` ja `varianssi`,
#' jotka siis ovat `amount` muuttujasta lasketutut mediaani ja varianssi.**
#' 
#+ vastaus25
plot(cars)

#' yhteenvedoissa näppäriä lisäfunktioita ovat mm. seuraavat
#' 
#' min(), max() # Minimum and maximum values
#' mean() # Mean value
#' median() # Median value
#' sum() # Sum of values
#' var, sd() # Variance and standard deviation of a vector
#' first() # First value in a vector
#' last() # Last value in a vector
#' nth() # Nth value in a vector
#' n() # The number of values in a vector
#' n_distinct() # The number of distinct values in a vector
#' 
#' **Miten saat `pollution``-datan `city`-muuttujan erilisten arvojen määrän?**
#+ vastaus26
plot(cars)


#' # Ryhmittäiset analyysit
#' 
#' Datan ryhmittely tapahtuu `dplyr::group_by()`-funktiolla.
#' 
#' Kun ryhmittelemme `pollution`-aineiston `city`-muuttujan mukaan ryhmitellyn `tibble`:n, jossa lukee
#' `Groups: city [3]`
#' 
#' ``
#+demo1, eval=FALSE
pollution %>% group_by(city)
#> Source: local data frame [6 x 3]
#> Groups: city [3]

#> city  size amount
#> <chr> <chr>  <dbl>
#> New York large     23
#> New York small     14
#> London large     22
#> London small     16
#> Beijing large    121
#> Beijing small     56

#' Jos ryhmittelet datasi ryhmittäisiä analyysejä varten, muista poistaa ryhmitys funktiolla `ungroup(x)`,
#' jotta ryhmittely ei häiritse seuraavia analyysejä.
#' 
#' **Ryhmittely on jo aikaisemmista kerroista osin tuttua, mutta laske vielä `pollution`-datasta 
#' kullekin kaupungille `amount`-muuttujan keskiarvo, summa ja tapausten määrä.**
#+ vastaus27
plot(cars)



#' Jatketaan vielä ryhmittelyä. Otetaan käyttöön vanha tuttu malesdata, jonka
#' saatte kätevästi käyttöön komennolla
#' `malesdata <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/malesdata.RDS")))`
#' 
#' **Ryhmittele aineisto `industry`-muuttujan mukaan, laske ryhmien palkan `wage` mediaani ja 
#' valitse vain ryhmät joiden keskipalkka on yhtä suuri tai suurempi kuin 1.6**
#+vastaus28
plot(cars)

#' # Datojen yhdistäminen
#' 
#' dplyr-paketin eri `join`-funktiot ovat näppäriä base.R `merge`-funktion ohella datojen yhdistämiseen
#' 
#' Otetaan harjoituksen alussa tehdyt datat `songs` ja `artists` ja printataan ne alkuun konsoliin
#+ demo2, eval=FALSE
songs
#> # A tibble: 4 × 2
#> song  name
#> <chr> <chr>
#> Across the Universe  John
#> Come Together  John
#> Hello, Goodbye  Paul
#> Peggy Sue Buddy

artists
#> # A tibble: 4 × 2
#>name  plays
#><chr>  <chr>
#> George  sitar
#> John guitar
#> Paul   bass
#> Ringo  drums

#' **liitä data ensin artistin nimen mukaan funktiolla `left_join()`**
#+vastaus33
plot(cars)

#' **Sitten liitä data toisiinsa kaikilla seuraavilla funktioilla**
#' - `inner_join()`
#' - `semi_join()`
#' - `anti_join()`
#' - `full_join()`
#+vastaus34
plot(cars)

#' **Huomaatko mitkä ovat eri funktioiden erot? (Valmistaudu kertomaan vieruskaverille luennolla!)**
#+vastaus35
plot(cars)

#' Datojen yhdistämiseen on myös erinomaisia lisäfunktioita dplyr-paketissa kuten
#' 
#' - dplyr::bind_cols(x,y)
#' - dplyr::bind_rows
#' - dplyr::union
#' - dplyr::intersect
#' - dplyr::setdif
#' 
#' Tutustu niihin jos aikaa jää!
#' 
#' *** 
#' # Jokeri 1: Tieteellisten julkaisujen hintatietojen perkaaminen
#' 
#' Alkukesästä avattiin [kattavaa julkaisijakohtaista tietoa korkeakoulujen ja tutkimusorganisaatioiden 
#' maksamista tilaushinnoista usean vuoden ajalta](http://louhos.github.io/news/2016/06/13/FOI). 
#' Tämä data on ladattavissa täältä: <http://avaa.tdata.fi/documents/kuhiti/Publisher_Costs_FI_Full_Data.csv>
#' 
#' **Lue csv-tiedosto R:ään ja laske Turun yliopiston (University of Turku) vuosittaiset kustannukset**
#+ vastaus30
plot(cars)


#' **Valitse kaikki ammattikorkeakoulut ja laske niiden vuosittaiset kustannukset**
#+ vastaus31
plot(cars)

#' **Valitse kaikki julkaisijat (muuttuja Publisher.Supplier) joissa on sana `American` (**muista `grepl`-funktio) ja 
#' laske niiden vuosittaiset kustannukset eri *yliopistoissa* **
#' 
#+ vastaus32
plot(cars)


#' 
#' # Jokeri 2: Sikoja pohjoismaissa
#' 
#' Lataa FAOSTAT-tietokannasta zipattu .csv data osoitteesta <http://faostat3.fao.org/faostat-bulkdownloads/Production_Livestock_E_All_Data.zip> ja 
#' pura se koneellasi (funktio `unzip()`), ja lataa csv-koneellesi. (hätätapauksessa data löytyy myös: http://courses.markuskainu.fi/utur2016/database/Production_Livestock_E_All_Data.csv)
#' 
#' **Poista datasta ensin kaikki ns. *flag* muuttujat eli muotoa Y1961F tai Y2010F olevat muuttujat (jotka loppuvat F:ään). 
#' Sitten käännä data pitkään muotoon**
#' 
#' Kun data on oikeassa formaatissa **selvitä montako sikaa pohjoismaissa on tuotettu vuosittain vuosina 2000-2012 ja 
#' mikä pohjoismaiden sikatuotannon osuus on ollut koko maailman tuotetuista sioista noina vuosina**
#+vastaus98
plot(cars)

#' 
#' 
#' 
#' # Jokeri 3: Sotkuisen eriarvoisuusdatan perkaamista
#' 
#' [Luxembourg income study](http://www.lisdatacenter.org/) on julkaissut eriarvoisuusdatan yhdessä NY Timesin kanssa nimeltä 
#' [LIS / New York Times Income Distribution Database (2016)](http://www.lisdatacenter.org/news-and-events/new-york-times-launches-new-site-the-upshot-with-study-based-on-lis-data/), 
#' jonka pohjalta on tehty mm. juttu <http://www.nytimes.com/2014/04/23/upshot/the-american-middle-class-is-no-longer-the-worlds-richest.html> sekä 
#' itse datasta on kirjoitettu tämä <http://www.nytimes.com/2014/04/23/upshot/about-the-data.html> ja tämä <http://www.nytimes.com/2014/04/23/upshot/back-story-how-we-found-the-income-data.html>
#' 
#' Data itsessään on monivälilehtisenä ekselinä täällä <http://www.lisdatacenter.org/wp-content/uploads/resources-other-nyt.xlsx>. 
#' Viidenneltä välilehdeltä löytyy keskiarvoistettuja ostovoimapariteettikorjattuja tulodesiilien rajoja sekä a) per capita 
#' että b) ekvivalisoituna kotitalouden jäsentä kohtaan sekä c) koko kotitalouden tasolla.
#' 
#' Alla on esimerkki siitä millaiseen muoton ko. 5 välilehden data tulisi saada.  
#' 
#' |data |country   | year|class                                          |variable |value |
#' |:----|:---------|----:|:----------------------------------------------|:--------|:-----|
#' |LIS  |Australia | 1981|Mean Income per Capita (PPP), 2011 PPP dollars |Decile 1 |3556  |
#' |LIS  |Australia | 1985|Mean Income per Capita (PPP), 2011 PPP dollars |Decile 1 |3612  |
#' |LIS  |Australia | 1989|Mean Income per Capita (PPP), 2011 PPP dollars |Decile 1 |3601  |
#' |LIS  |Australia | 1995|Mean Income per Capita (PPP), 2011 PPP dollars |Decile 1 |3377  |
#' |LIS  |Australia | 2001|Mean Income per Capita (PPP), 2011 PPP dollars |Decile 1 |3813  |
#' |LIS  |Australia | 2003|Mean Income per Capita (PPP), 2011 PPP dollars |Decile 1 |3984  |
#' 
#' **Miten saan ladattua ko. datan ja muokattua sen ylläolevan esimerkin mukaiseen tidy-muotoon?**
#+ vastaus99
plot(cars)


