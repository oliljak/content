#' ---
#' title: "Kotitehtävä 5"
#' author: utu_tunnus
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session5_communicate_kotitehtava.R)

#+ setup
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))

#' # Viides kotitehtävä eli harjoitustyö
#' 
#' 5. kotitehtävän tarkoituksena on soveltaa itsenäisesti kurssilla opittuja data-analyysin vaiheita
#' ja laatia itseä kiinnostavasta aiheesta suppea data-analyysi, jossa
#' 
#' 1. haetaan dataa verkosta joko tiedostosta tai rajapinnasta
#' 2. dataja puhdistetaan ja yhdistellään toisiin
#' 3. datoja muokataan ja tehdään niihin uusia muuttujia ja lasketaan yhteenvetoja
#' 4. analyysin tuloksia selvitetään tualukoilla, kuvioilla ja mahdollisesti myös tilastollisilla malleilla
#' 5. koko prosessi kirjoitetaan yhtenä toistettavana .Rmd-dokumenttina (Laadi raportti tähän tiedostoon!)
#' 
#' 
#' # Mahdollisia aiheita
#' 
#' ## Verotiedot
#' 
#' Viime päivinä paljon mielenkiintoa on saanut verotiedot. Jarno Tuimala kirjoitti kaksi blogipostausta
#' aiheesta r-ohjelmointi.org -blogiin:
#' 
#' - Suuri veropäivä <http://www.r-ohjelmointi.org/?p=2828>
#' - Suuri veropäivä – osa 2: Power BI -raportti vuoden 2014 julkisista verotiedoista <http://www.r-ohjelmointi.org/?p=2837>
#' 
#' Etenkin kakkososan koodin alussa olevalla Tilastokeskuksen rajapinnasta dataa hakevalla koodilla saat
#'  jo sellaisen aineiston, jonka pohjalta voit myös tehdä gradun ja väitellä.
#'  
#'  Saat datan helpoiten komennolla `load(url("http://courses.markuskainu.fi/utur2016/database/verotiedot2015.RData"))`
#'  
#'  ## Usan presidentinvaalit
#'  
#'  Presidentinvaalit ovat tiistaina ja R-yhteisö vääntää kuumeisesti erilaisia ennusteita 
#'  mielipidetiedusteluihin perustuen. Tiistain jälkeen saatavilla 
#'  
#'  - FiveThirtyEight's polling data for the US Presidential election <http://ellisp.github.io/blog/2016/10/29/538-pollsters>
#'  - Alkuperäinen juttu <http://projects.fivethirtyeight.com/2016-election-forecast/?ex_cid=rrpromo#plus>
#'  - mielipidetiedusteludataa etenkin Yhdysvalloista saa [pollstR](https://cran.r-project.org/web/packages/pollstR/vignettes/introduction.html)-paketilla
#'  jolla voit käyttää Huffington Postin [Pollster API](http://elections.huffingtonpost.com/pollster/api)
#'  
#'  
#'  ## Avointa dataa Turusta
#'  
#'  - <http://www.lounaistieto.fi/tietopalvelut/avoimet-aineistot/>
#'  - <https://www.avoindata.fi/data/en/organization/turku>
#'  
#'  

