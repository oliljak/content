---
title: "R-kurssin loppuharjoitus"
author: "Olli Jakonen, oliljak@utu.fi"
date: "18 marraskuuta 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, cache = TRUE)
```

```{r, messages = FALSE, echo = FALSE}
library(tidyr)
library(ggplot2)
library(plyr)
library(dplyr)
library(knitr)
library(forcats)
```

> Palautus on myöhässä, koska knittaamisessa kesti järkyttävän kauan, enkä aluksi keksinyt mistä se johtui. Lopulta, kun knittaus valmistui, outputiin oli tulostettu koko 1,8 miljoonan havainnon lista. Ensi kerralla olen viisaampi. Toivottavasti kelpaan vielä R-mukin arvontaan.

**Ladataan verotiedot2015-data**

```{r, cache=TRUE}
verot <- load("C:/Users/uneun/OneDrive/Documents/Yliopisto/Tilastotiede/verotiedot2015.Rdata")
```




 
# Muuttujamuunnokset

**Muutetaan alue_sukupuoli-muuttujan sarakkeen vaikeaselkoinen nimi**

```{r, eval = FALSE, messages = FALSE}
unique(alue_sukupuoli)
```

```{r}
alue_sukupuoli2 <- alue_sukupuoli %>%
  mutate(Era = `ErÃ¤`) %>%
  select(-`ErÃ¤`)
```





**Tarkastellaan datassa mukana olevia alueita, Turku ja Tampere vaikuttavat erityisen mielenkiintoisilta vertailukohteilta.**

```{r, eval = FALSE, messages = FALSE}
unique(alue_sukupuoli2$Alue)
alue_sukupuoli2
```





# Datan muokkaus

**Suodatetaan alue_sukupuoli-datasta Turun ja Tampereen työttömyysturvaetuudet.Tehdään objektit erikseen miesten ja naisten keskiarvoille ja miesten ja naisten mediaanille.**

```{r}
keskiarvo_miehet <- alue_sukupuoli2 %>% filter(Alue %in% c("Turku", "Tampere"),
                                            Era %in% "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Keskiarvo",
                                            Sukupuoli %in% "Miehet") %>% 
                                            select(Alue, values, Sukupuoli) %>%
                                            spread(key = Alue, value = values)

```

```{r}
keskiarvo_miehet
```

```{r}
keskiarvo_naiset <- alue_sukupuoli2 %>% filter(Alue %in% c("Turku", "Tampere"),
                                            Era %in% "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Keskiarvo",
                                            Sukupuoli %in% "Naiset") %>%
                                            select(Alue, values, Sukupuoli) %>%
                                            spread(key = Alue, value = values)
```

```{r}
keskiarvo_naiset
```

```{r}
mediaani_miehet <- alue_sukupuoli2 %>% filter(Alue %in% c("Turku", "Tampere"),
                                            Era %in% "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Mediaani",
                                            Sukupuoli %in% "Miehet") %>%
                                            select(Alue, values, Sukupuoli) %>%
                                            spread(key = Alue, value = values)
```

```{r}
mediaani_miehet
```

```{r}
mediaani_naiset <- alue_sukupuoli2 %>% filter(Alue == "Turku" | Alue == "Tampere",
                                            Era == "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Mediaani",
                                            Sukupuoli %in% "Naiset") %>%
                                            select(Alue, values, Sukupuoli) %>%
                                            spread(key = Alue, value = values)
```

```{r}
mediaani_naiset
```





**Sama ilman spread-funktiota**

```{r}
keskiarvo_miehet2 <- alue_sukupuoli2 %>% filter(Alue %in% c("Turku", "Tampere"),
                                            Era %in% "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Keskiarvo",
                                            Sukupuoli %in% "Miehet") %>% 
                                            select(Alue, values, Sukupuoli)
```


```{r}
keskiarvo_miehet2
```

```{r}
keskiarvo_naiset2 <- alue_sukupuoli2 %>% filter(Alue %in% c("Turku", "Tampere"),
                                            Era %in% "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Keskiarvo",
                                            Sukupuoli %in% "Naiset") %>%
                                            select(Alue, values, Sukupuoli)
```

```{r}
keskiarvo_naiset2
```

```{r}
mediaani_miehet2 <- alue_sukupuoli2 %>% filter(Alue %in% c("Turku", "Tampere"),
                                            Era %in% "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Mediaani",
                                            Sukupuoli %in% "Miehet") %>%
                                            select(Alue, values, Sukupuoli)
```

```{r}
mediaani_miehet2
```

```{r}
mediaani_naiset2 <- alue_sukupuoli2 %>% filter(Alue == "Turku" | Alue == "Tampere",
                                            Era == "4.2.2 - TyÃ¶ttÃ¶myysturvaetuudet",
                                            Tunnusluvut %in% "Mediaani",
                                            Sukupuoli %in% "Naiset") %>%
                                            select(Alue, values, Sukupuoli)
```

```{r}
mediaani_naiset2
```





**Yhdistetään naisten ja miesten keskiarvot ja mediaanit yhteen objektiin.**

```{r}
keskiarvo <- bind_rows(keskiarvo_miehet, keskiarvo_naiset)
mediaani <- bind_rows(mediaani_miehet, mediaani_naiset)

keskiarvo2 <- bind_rows(keskiarvo_miehet2, keskiarvo_naiset2)
mediaani2 <- bind_rows(mediaani_miehet2, mediaani_naiset2)
```





# Graafinen tarkastelu

**Tehdään keskiluvuista eli mediaanista ja keskiarvosta havainnollistavat pylväsdiagrammit**

```{r, message = FALSE, warning = FALSE}
mediaani_plot <- ggplot(mediaani2, aes(x = Sukupuoli, y = values, colour = factor(Sukupuoli), fill = Sukupuoli)) +
  geom_bar(stat = "identity") +
  theme(axis.title.y = element_text(size=8), axis.title.x = element_text(size=10)) +
  facet_wrap(~Alue, nrow = 2, ncol = 2, scales = "fi") +
  labs(x = "Sukupuoli", y = "Euroa", colour = "Sukupuoli") +
  ggtitle("Miesten ja naisten \ntyöttömyysturvaetuuden mediaani\nTurussa ja Tampereella") +
  theme(plot.title = element_text(face = "bold", size = 8))

print(mediaani_plot)
```

```{r, message = FALSE, warning = FALSE}
keskiarvo_plot <- ggplot(keskiarvo2, aes(x = Sukupuoli, y = values, colour = factor(Sukupuoli), fill = Sukupuoli)) +
  geom_bar(stat = "identity") +
  theme(axis.title.y = element_text(size=8), axis.title.x = element_text(size=10)) +
  facet_wrap(~Alue, nrow = 2, ncol = 2, scales = "fi") +
  labs(x = "Sukupuoli", y = "Euroa", colour = "Sukupuoli") +
  ggtitle("Miesten ja naisten \ntyöttömyysturvaetuuden keskiarvo\nTurussa ja Tampereella") +
  theme(plot.title = element_text(face = "bold", size = 8))

print(keskiarvo_plot)
```

---

# Tulokset

Kuvioista voidaan päätellä seuraavaa:

1. Sekä Tampereella että Turussa miesten työttömyysturvan keskiarvo ja mediaani ovat korkeampia kuin naisten.
2. Ero miesten ja naisten välillä on hieman pienempi, jos tarkastellaan keskiarvoa
3. Tampereella työttömyysturvaa nostetaan keskimäärin ja mediaaniarvon mukaisesti enemmän kuin Turussa.